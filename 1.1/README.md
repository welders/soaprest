# Assignment 1.1
### Requirements
- java 1.8.*

### Build
    gradle build
    
### Run
    java -jar server/build/libs/server.jar
    java -jar client/build/libs/client.jar
    java -jar client_topdown/build/libs/client_topdown.jar