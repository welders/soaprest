import webService.CalculatorService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class CalculatorClient{

    public static void main(String... args) throws MalformedURLException {
        URL url = new URL("http://localhost:9998/web-service/calculator?wsdl");
        QName qname = new QName("http://welders/", "CalculatorServiceImplService");
        Service service = Service.create(url, qname);
        CalculatorService calculator = service.getPort(CalculatorService.class);

        System.out.println("20 + 22 = " + calculator.add(20., 22.));
        System.out.println("47 - 5 = " + calculator.sub(47., 5.));
        System.out.println("10.5 * 4 = " + calculator.mul(10.5, 4.));
        System.out.println("84 / 2 = " + calculator.div(84., 2.));
    }
}