package topdown;

import webService.CalculatorService;

import javax.xml.ws.Service;

public class CalculatorClientRun {

    public static void main(String... args) {
        Service calculatorService = new CalculatorServiceImplService();

        CalculatorService calculator = calculatorService.getPort(CalculatorService.class);

        System.out.println("20 + 22 = " + calculator.add(20., 22.));
        System.out.println("47 - 5 = " + calculator.sub(47., 5.));
        System.out.println("10.5 * 4 = " + calculator.mul(10.5, 4.));
        System.out.println("84 / 2 = " + calculator.div(84., 2.));
    }
}
