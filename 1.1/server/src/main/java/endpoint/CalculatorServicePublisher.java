package endpoint;

import webService.CalculatorServiceImpl;

import javax.xml.ws.Endpoint;

public class CalculatorServicePublisher {

    public static void main(String... args) {
        Endpoint.publish("http://localhost:9998/web-service/calculator", new CalculatorServiceImpl());
    }
}
