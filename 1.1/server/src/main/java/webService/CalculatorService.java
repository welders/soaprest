package webService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "webService.CalculatorService", targetNamespace = "http://welders/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface CalculatorService {

    @WebMethod
    double add(double arg0, double arg1);

    @WebMethod
    double sub(double arg0, double arg1);

    @WebMethod
    double mul(double arg0, double arg1);

    @WebMethod
    double div(double arg0, double arg1);
}
