# Assignment 1.2

## Requirements
- python 3.6
- pipenv (`pip install pipenv`)

## Install
    pipenv install
    
## Run Server (runs on localhost:9998)
    pipenv run python shortener_service.py

## Example Shorten URL
    curl -d "url=http://www.google.com" -X POST "http://localhost:9998"

## Run Unit Tests
    pipenv run python shortener_client.py

## Run Demo
    pipenv run python demo.py