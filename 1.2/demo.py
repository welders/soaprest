from shortener_service import app

app = app.test_client()
app.testing = True

shortened = app.post('/', data={'url': "http://www.google.com"})    # good
app.post('/', data={'url': "bla"})                                  # bad
result = app.get('/')
print(result.data)
app.put('/' + shortened.data.decode('utf-8'), data={'url': "http://www.amazon.com"})
result = app.get('/')
print(result.data)
result = app.delete('/')
print(result.data)
