import unittest
from shortener_service import app, shorten_url


class ShortenerServiceTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
        self.valid_url = 'https://www.google.com'
        self.invalid_url = 'bla'

    def tearDown(self):
        pass

    def test_home_post_valid(self):
        result = self.app.post('/', data={'url': self.valid_url})

        self.assertEqual(201, result.status_code)
        self.assertEqual(shorten_url(self.valid_url) - 1, int(result.data))

    def test_home_post_invalid(self):
        result = self.app.post('/', data={'url': self.invalid_url})

        self.assertEqual(400, result.status_code)
        self.assertEqual('"invalid URL"\n', result.data.decode('utf-8'))

        result = self.app.post('/', data=None)

        self.assertEqual(400, result.status_code)
        self.assertEqual('"error"\n', result.data.decode('utf-8'))

    def test_home_get(self):
        result = self.app.get('/')

        self.assertEqual(200, result.status_code)
        self.assertEqual('{}\n', result.data.decode('utf-8'))

    def test_home_delete(self):
        result = self.app.delete()

        self.assertEqual(204, result.status_code)
        self.assertEqual('', result.data.decode('utf-8'))

    def test_id_get_valid(self):
        response = self.app.post('/', data={'url': self.valid_url})

        result = self.app.get('/' + response.data.decode('utf-8'))

        self.assertEqual(301, result.status_code)
        self.assertEqual('"https://www.google.com"\n', result.data.decode('utf-8'))

    def test_id_get_invalid(self):
        result = self.app.get('/' + self.invalid_url)

        self.assertEqual(404, result.status_code)
        self.assertEqual('null\n', result.data.decode('utf-8'))

    def test_id_put_invalid(self):
        result = self.app.put('/' + self.invalid_url, data={'url': self.valid_url})
        self.assertEqual(404, result.status_code)
        self.assertEqual('null\n', result.data.decode('utf-8'))

        response = self.app.post('/', data={'url': self.valid_url})

        result = self.app.put('/' + str(int(response.data)))
        self.assertEqual(400, result.status_code)
        self.assertEqual('"error"\n', result.data.decode('utf-8'))

        result = self.app.put('/' + str(int(response.data)), data={'url': self.invalid_url})
        self.assertEqual(400, result.status_code)
        self.assertEqual('"invalid URL"\n', result.data.decode('utf-8'))

    def test_id_put_valid(self):
        response = self.app.post('/', data={'url': self.valid_url})
        result = self.app.put('/' + response.data.decode('utf-8'), data={'url': self.valid_url + '/search'})

        self.assertEqual(200, result.status_code)
        self.assertEqual('null\n', result.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
