from flask import Flask, Blueprint
from flask_restful import Resource, Api, reqparse
import re

app = Flask(__name__)
api_blueprint = Blueprint('api', __name__)
api = Api(api_blueprint)

shortened_urls = {}

parser = reqparse.RequestParser()
parser.add_argument('url')


# inspired from django
def is_valid_url(url):
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    return url is not None and regex.search(url)


# creates an id from the url which is not guaranteed to be unique
def hash_url(url):
    return abs(hash(url)) % (10 ** 8)


# creates a unique id to the shortened_urls.keys() from the url
def shorten_url(url):
    shortened_url = hash_url(url)

    while shortened_url in shortened_urls.keys():
        shortened_url += 1

    return shortened_url


class ShortenerId(Resource):
    @staticmethod
    def get(id):
        # TODO: this doesn't seem like the best way to do it but there's a \n at the end which int() gets rid off
        try:
            id = int(id)
        except ValueError:
            return None, 404

        if id not in shortened_urls:
            return None, 404
        else:
            return shortened_urls[id], 301

    @staticmethod
    def put(id):
        try:
            id = int(id)
        except ValueError:
            return None, 404

        if id not in shortened_urls:
            return None, 404

        args = parser.parse_args()
        url = args['url']

        if not url:
            return "error", 400
        elif not is_valid_url(url):
            return "invalid URL", 400
        else:
            shortened_urls[id] = url

            return None, 200

    @staticmethod
    def delete(id):
        if id not in shortened_urls:
            return None, 404
        else:
            shortened_urls.pop(id)

            return None, 204


class Shortener(Resource):
    @staticmethod
    def get():
        return shortened_urls, 200

    @staticmethod
    def post():
        args = parser.parse_args()
        url = args['url']

        if not url:
            return 'error', 400
        elif not is_valid_url(url):
            return 'invalid URL', 400
        else:
            # if url already shortened, return its id
            for key, value in shortened_urls.items():
                if value == url:
                    return key, 201

            url_id = shorten_url(url)
            shortened_urls[url_id] = url

            return url_id, 201

    @staticmethod
    def delete():
        shortened_urls.clear()

        return None, 204


api.add_resource(Shortener, '/')
api.add_resource(ShortenerId, '/<string:id>')
app.register_blueprint(api_blueprint)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9998)
