# Assignment 1.1
### Requirements
- java 1.8.*

### Build
    cd 1.1
    gradle build
    
### Run
    java -jar server/build/libs/server.jar
    java -jar client/build/libs/client.jar
    java -jar client_topdown/build/libs/client_topdown.jar

# Assignment 1.2

## Requirements
- python 3.6
- pipenv (`pip install pipenv`)

## Install
    cd 1.2
    pipenv install
    
## Run
    pipenv run python shortener_service.py
    pipenv run python shortener_client.py
